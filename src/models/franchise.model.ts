export interface FranchiseModel {
  id: string;
  name: string;
  email?: string;
}
