export interface FleetChargerModel {
  id: string;
  name: string;
  chargerId: string;
}
