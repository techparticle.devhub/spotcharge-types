export interface VendorModel {
  id: string;
  name: string;
}
