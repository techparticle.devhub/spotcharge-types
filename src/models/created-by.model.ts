export interface CreatedByModel {
  id: string;
  name: string;
}

export interface PerformedBy {
  id: string;
  name: string;
}
