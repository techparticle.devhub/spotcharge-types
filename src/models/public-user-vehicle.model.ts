export interface PublicUserVehicle {
    id: string;
    model: string;
    registrationNo: string;
    image: string;
    year: number;
    name: string;
    batteryCapacity: number;
}