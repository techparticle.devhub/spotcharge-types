export interface CountryModel {
  name: string;
  code: string;
  currencyCode: string;
  currencySymbol: string;
}
