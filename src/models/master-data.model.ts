export interface MasterData {
  title: string;
  value: number | string;
}
