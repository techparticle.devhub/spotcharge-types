export interface ConnectorTypeModel {
  id: string;
  name: string;
  icon?: string;
}
