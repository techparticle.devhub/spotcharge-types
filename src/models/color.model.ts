export interface ColorModel {
    id: number,
    primary: string;
    secondary: string;
    tertiary: string;
}
