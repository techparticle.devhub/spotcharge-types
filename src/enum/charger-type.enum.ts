export enum ChargerType {
    Static = 'static',
    Dynamic = 'dynamic'
}