export enum PointType {
    DC = 'dc',
    AC = 'ac',
    Hybrid = 'hybrid'
}