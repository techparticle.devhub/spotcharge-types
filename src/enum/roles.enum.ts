export enum Roles {
  SuperAdmin = 'super_admin',
  FranchiseAdmin = 'franchise_admin',
  FleetUser = 'fleet_user',
  FleetOperator = 'fleet_operator',
}
