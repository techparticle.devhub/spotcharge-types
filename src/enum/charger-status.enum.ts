export enum ChargerStatus {
  Online = 'online',
  Offline = 'offline'
}
