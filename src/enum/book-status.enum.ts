export enum BookingStatus {
    CONFIRMED = "confirmed",
    CANCELLED = "cancelled",
    COMPLETED = "completed",
    PENDING = "pending",
    IN_PROGRESS = "in_progress",
    PAUSED = "paused",
}
