export enum PaymentStatus {
    Paid = 'paid',
    UnPaid = 'unPaid',
    Failed = 'failed',
    Prepayment = 'prepayment',
    PartialPaid = 'partialPaid',
}
