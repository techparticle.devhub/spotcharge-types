export enum EntityType  {
  client = 'client',
  business = 'business',
  system = 'system',
  superAdmin = 'superAdmin',
  franchise = 'franchise',
  fleetApp = 'fleetApp',
  publicApp = 'publicApp',
  fleetApi = 'fleetApi',
  publicApi = 'publicApi',
}
