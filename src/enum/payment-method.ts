export enum PaymentMethod {
    Wallet = 'wallet',
    TopUp = 'top_up',
    PartiallyWallet = 'partially_wallet'
}
