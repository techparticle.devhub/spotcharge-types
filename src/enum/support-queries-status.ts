export enum SupportQueriesStatus {
    Open = "open",
    InProgress = "in-progress",
    Resolved = "resolved"
}
