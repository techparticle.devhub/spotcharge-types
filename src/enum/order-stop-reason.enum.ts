export enum OrderStopReason {
    EmergencyStop = "EmergencyStop",
    EvDisconnected = "EVDisconnected",
    HardReset = "HardReset",
    Local = "Local",
    Other = "Other",
    PowerLoss = "PowerLoss",
    Reboot = "Reboot",
    Remote = "Remote",
    SoftReset = "SoftReset",
    UnlockCommand = "UnlockCommand",
    DeAuthorized = "DeAuthorized"
}