// eslint-disable-next-line no-shadow
export enum ColorTheme {
  Dark = 'dark',
  Light = 'light',
}
