export enum TransactionReason {
    REFUND = 'refund',
    REWARD = 'reward',
    PROMOTION = 'promotion',
    COMPENSATION = 'compensation',
    BONUS = 'bonus',
    DEPOSIT = 'deposit',
    WITHDRAWAL = 'withdrawal',
    CHARGING = 'charging',
    ADD_BALANCE = 'add balance',
    BOOKING = 'booking',
    BANK_REFUND = 'bank refund'
}
