export enum TimeUnit {
    Minute = 'minute',
    Hour = 'hour',
}