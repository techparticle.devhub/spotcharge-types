export enum TaxCategory {
    Individual = 'individual',
    Group = 'group',
}