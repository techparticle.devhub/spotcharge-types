export enum OrderState {
    Initiated = 'initiated',
    Started = 'started',
    InProgress = 'inProgress',
    Completed = 'completed',
}
