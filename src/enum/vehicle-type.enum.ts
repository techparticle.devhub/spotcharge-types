export enum VehicleType {
    Car = 'car',
    Bike = 'bike',
    Rikshaw = 'rikshaw',
    Pickup = 'Pickup'
}