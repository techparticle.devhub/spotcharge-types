export interface CountryModel {
    name: string;
    code: string;
    currencyCode: string;
    currencySymbol: string;
}
//# sourceMappingURL=country.model.d.ts.map