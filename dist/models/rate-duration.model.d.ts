import { TimeUnit } from "../enum";
export interface RateDuration {
    label: string;
    value: number;
    type: TimeUnit;
}
//# sourceMappingURL=rate-duration.model.d.ts.map