export interface ConnectorTypeModel {
    id: string;
    name: string;
    icon?: string;
}
//# sourceMappingURL=connector-type.model.d.ts.map