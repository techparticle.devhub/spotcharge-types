export interface FranchiseModel {
    id: string;
    name: string;
    email?: string;
}
//# sourceMappingURL=franchise.model.d.ts.map