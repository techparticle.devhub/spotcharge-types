export interface MasterData {
    title: string;
    value: number | string;
}
//# sourceMappingURL=master-data.model.d.ts.map