export interface CreatedByModel {
    id: string;
    name: string;
}
export interface PerformedBy {
    id: string;
    name: string;
}
//# sourceMappingURL=created-by.model.d.ts.map