export interface ColorModel {
    id: number;
    primary: string;
    secondary: string;
    tertiary: string;
}
//# sourceMappingURL=color.model.d.ts.map