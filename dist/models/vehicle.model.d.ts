import { BrandModel } from "./brand.model";
export interface VehicleModel {
    id: string;
    model: string;
    brand: BrandModel;
    type: string;
}
//# sourceMappingURL=vehicle.model.d.ts.map