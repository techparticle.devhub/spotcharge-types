export interface AddressApiModel {
    addressLine1: string;
    state: StateModel;
    street: string;
    country: string;
    postalCode: string;
    city: string;
    location: any;
}
export interface StateModel {
    code: string;
    name: string;
}
//# sourceMappingURL=address.api.model.d.ts.map