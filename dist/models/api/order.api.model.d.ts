import { CreatedByModel } from "../created-by.model";
import { Timestamp } from "../timestamp";
import { FleetModel } from "../fleet.model";
import { FranchiseModel } from "../franchise.model";
import { OrderType } from "../../enum";
import { ChargingTransactionRecords } from "../charging-transaction";
import { FleetVehicleModel } from "../fleet-vehicle.model";
export interface OrderApiModel {
    orderId: string;
    id: string;
    chargerId: string;
    chargerName: string;
    startTime: Timestamp;
    endTime: Timestamp;
    chargingDate: Timestamp;
    fleet: FleetModel;
    franchise: FranchiseModel;
    highestAmpere?: number;
    stopReason: string;
    orderNo: number;
    transactionId: number;
    currentTransactionId: number;
    transaction: ChargingTransactionRecords;
    connectorNo: number;
    unitConsumed: number;
    meterStart: number;
    meterStop: number;
    customerName: string;
    state: string;
    userId: string;
    unitRate: number;
    fleetVehicle: FleetVehicleModel;
    orderType: OrderType;
    totalConsumedUnits: number;
    totalAmount: number;
    createdBy: CreatedByModel;
    updatedBy: CreatedByModel;
    deletedBy: CreatedByModel | null;
    createdAt: Timestamp;
    updatedAt: Timestamp;
    deletedAt: Timestamp | null;
    isDeleted: boolean;
}
//# sourceMappingURL=order.api.model.d.ts.map