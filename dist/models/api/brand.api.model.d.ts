import { CreatedByModel } from "../created-by.model";
import { Timestamp } from "../timestamp";
export interface BrandApiModel {
    id: string;
    name: string;
    logo: string | null;
    showOnFleet: boolean;
    showOnPublic: boolean;
    createdBy: CreatedByModel;
    updatedBy: CreatedByModel;
    deleteBy: CreatedByModel;
    createdAt: Timestamp;
    updatedAt: Timestamp;
    deletedAt: Timestamp | null;
    isDeleted: boolean;
}
//# sourceMappingURL=brand.api.model.d.ts.map