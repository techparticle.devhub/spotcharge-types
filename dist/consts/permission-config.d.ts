export declare const permissionConfig: {
    VIEW_FLEET_DASHBOARD: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_DASHBOARD: {
        key: string;
        formattedPermission: string;
    };
    LIST_FRANCHISES: {
        key: string;
        formattedPermission: string;
    };
    ADD_FRANCHISE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FRANCHISE_SETTING: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FRANCHISE_ACCOUNT_SETTING: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FRANCHISE_PROFILE: {
        key: string;
        formattedPermission: string;
    };
    FRANCHISE_BILLING: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FRANCHISE_FLEET_INVOICE: {
        key: string;
        formattedPermission: string;
    };
    FRANCHISE_ORDERS: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FRANCHISE_FLEET_ORDER: {
        key: string;
        formattedPermission: string;
    };
    FRANCHISE_SUBSCRIPTION: {
        key: string;
        formattedPermission: string;
    };
    LIST_FLEETS: {
        key: string;
        formattedPermission: string;
    };
    ADD_FLEET: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FLEET: {
        key: string;
        formattedPermission: string;
    };
    DELETE_FLEET: {
        key: string;
        formattedPermission: string;
    };
    LIST_FLEET_USERS: {
        key: string;
        formattedPermission: string;
    };
    ADD_FLEET_USER: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FLEET_USER: {
        key: string;
        formattedPermission: string;
    };
    DELETE_FLEET_USER: {
        key: string;
        formattedPermission: string;
    };
    LIST_FLEET_OPERATORS: {
        key: string;
        formattedPermission: string;
    };
    ADD_FLEET_OPERATOR: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FLEET_OPERATOR: {
        key: string;
        formattedPermission: string;
    };
    DELETE_FLEET_OPERATOR: {
        key: string;
        formattedPermission: string;
    };
    LIST_FLEET_VEHICLES: {
        key: string;
        formattedPermission: string;
    };
    ADD_FLEET_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_FLEET_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    DELETE_FLEET_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    LIST_CHARGER_ERRORS: {
        key: string;
        formattedPermission: string;
    };
    VIEW_CHARGER_ERROR: {
        key: string;
        formattedPermission: string;
    };
    DOWNLOAD_CHARGER_ERROR_LOG: {
        key: string;
        formattedPermission: string;
    };
    LIST_FLEET_ORDER_SCHEDULES: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_ORDER_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    STOP_FLEET_ORDER_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    RESTART_FLEET_ORDER_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    LIST_PUBLIC_BOOKINGS: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_BOOKING: {
        key: string;
        formattedPermission: string;
    };
    START_PUBLIC_BOOKING: {
        key: string;
        formattedPermission: string;
    };
    CANCEL_PUBLIC_BOOKING: {
        key: string;
        formattedPermission: string;
    };
    ISSUE_BOOKING_REFUND: {
        key: string;
        formattedPermission: string;
    };
    LIST_PUBLIC_INVOICES: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_INVOICE: {
        key: string;
        formattedPermission: string;
    };
    DOWNLOAD_PUBLIC_INVOICE: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_INVOICE_DOWNLOAD_REQUESTS: {
        key: string;
        formattedPermission: string;
    };
    LIST_PUBLIC_USERS: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_USER: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_USER_SUMMARY: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_USER_DETAIL: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_USER_BOOKING: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_USER_INVOICE: {
        key: string;
        formattedPermission: string;
    };
    PUBLIC_USER_WALLET: {
        key: string;
        formattedPermission: string;
    };
    LIST_PUBLIC_SCHEDULES: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    RESUME_PUBLIC_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    STOP_PUBLIC_SCHEDULE: {
        key: string;
        formattedPermission: string;
    };
    LIST_CHARGERS: {
        key: string;
        formattedPermission: string;
    };
    ADD_CHARGER: {
        key: string;
        formattedPermission: string;
    };
    EDIT_CHARGER: {
        key: string;
        formattedPermission: string;
    };
    DELETE_CHARGER: {
        key: string;
        formattedPermission: string;
    };
    LIST_CHARGER_CAPACITIES: {
        key: string;
        formattedPermission: string;
    };
    ADD_CHARGER_CAPACITY: {
        key: string;
        formattedPermission: string;
    };
    EDIT_CHARGER_CAPACITY: {
        key: string;
        formattedPermission: string;
    };
    DELETE_CHARGER_CAPACITY: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_ORDER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_FLEET_ORDER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ORDERS_BY_FLEET_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ORDERS_BY_FLEET_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ORDERS_BY_USER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ORDERS_BY_USER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_PUBLIC_ORDER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_PUBLIC_ORDER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ORDER_BY_PUBLIC_USER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ORDER_BY_PUBLIC_USER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ORDER_BY_PUBLIC_CHARGER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ORDER_BY_PUBLIC_CHARGER_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_FLEET_VEHICLE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_VEHICLE_IN_OUT_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_FLEET_VEHICLE_IN_OUT_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ASSET_IN_OUT_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ASSET_IN_OUT_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_ASSET_IN_OUT_SUMMARY_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_ASSET_IN_OUT_SUMMARY_REPORT: {
        key: string;
        formattedPermission: string;
    };
    VIEW_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT: {
        key: string;
        formattedPermission: string;
    };
    LIST_CONNECTOR_TYPES: {
        key: string;
        formattedPermission: string;
    };
    ADD_CONNECTOR_TYPE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_CONNECTOR_TYPE: {
        key: string;
        formattedPermission: string;
    };
    DELETE_CONNECTOR_TYPE: {
        key: string;
        formattedPermission: string;
    };
    LIST_VENDORS: {
        key: string;
        formattedPermission: string;
    };
    ADD_VENDOR: {
        key: string;
        formattedPermission: string;
    };
    EDIT_VENDOR: {
        key: string;
        formattedPermission: string;
    };
    LIST_RFIDS: {
        key: string;
        formattedPermission: string;
    };
    ADD_RFID: {
        key: string;
        formattedPermission: string;
    };
    EDIT_RFID: {
        key: string;
        formattedPermission: string;
    };
    DELETE_RFID: {
        key: string;
        formattedPermission: string;
    };
    LIST_VEHICLES: {
        key: string;
        formattedPermission: string;
    };
    ADD_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    DELETE_VEHICLE: {
        key: string;
        formattedPermission: string;
    };
    LIST_BRANDS: {
        key: string;
        formattedPermission: string;
    };
    ADD_BRAND: {
        key: string;
        formattedPermission: string;
    };
    EDIT_BRAND: {
        key: string;
        formattedPermission: string;
    };
    DELETE_BRAND: {
        key: string;
        formattedPermission: string;
    };
    LIST_LOGS: {
        key: string;
        formattedPermission: string;
    };
    EXPORT_LOG_REPORTS: {
        key: string;
        formattedPermission: string;
    };
    LIST_SERVICE_CHARGES: {
        key: string;
        formattedPermission: string;
    };
    ADD_SERVICE_CHARGE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_SERVICE_CHARGE: {
        key: string;
        formattedPermission: string;
    };
    DELETE_SERVICE_CHARGE: {
        key: string;
        formattedPermission: string;
    };
    LIST_TAX_RATES: {
        key: string;
        formattedPermission: string;
    };
    ADD_TAX_RATE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_TAX_RATE: {
        key: string;
        formattedPermission: string;
    };
    DELETE_TAX_RATE: {
        key: string;
        formattedPermission: string;
    };
    LIST_GST: {
        key: string;
        formattedPermission: string;
    };
    ADD_GST: {
        key: string;
        formattedPermission: string;
    };
    EDIT_GST: {
        key: string;
        formattedPermission: string;
    };
    DELETE_GST: {
        key: string;
        formattedPermission: string;
    };
    LIST_SUPPORT_QUERY: {
        key: string;
        formattedPermission: string;
    };
    VIEW_SUPPORT_QUERY: {
        key: string;
        formattedPermission: string;
    };
    LIST_USER_ROLE: {
        key: string;
        formattedPermission: string;
    };
    ADD_USER_ROLE: {
        key: string;
        formattedPermission: string;
    };
    EDIT_USER_ROLE: {
        key: string;
        formattedPermission: string;
    };
    LIST_PERMISSION_GROUP: {
        key: string;
        formattedPermission: string;
    };
    ADD_PERMISSION_GROUP: {
        key: string;
        formattedPermission: string;
    };
    EDIT_PERMISSION_GROUP: {
        key: string;
        formattedPermission: string;
    };
    DELETE_PERMISSION_GROUP: {
        key: string;
        formattedPermission: string;
    };
    LIST_ADMIN_USERS: {
        key: string;
        formattedPermission: string;
    };
    ADD_ADMIN_USER: {
        key: string;
        formattedPermission: string;
    };
    EDIT_ADMIN_USER: {
        key: string;
        formattedPermission: string;
    };
    DELETE_ADMIN_USER: {
        key: string;
        formattedPermission: string;
    };
};
export type PermissionKeys = keyof typeof permissionConfig;
export type Permission = typeof permissionConfig[PermissionKeys] & {
    order: number;
};
//# sourceMappingURL=permission-config.d.ts.map