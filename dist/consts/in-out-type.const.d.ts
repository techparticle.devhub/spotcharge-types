export declare const InOutType: {
    readonly vehicle: "vehicle";
    readonly asset: "asset";
};
export type InOutKeys = keyof typeof InOutType;
//# sourceMappingURL=in-out-type.const.d.ts.map