"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleInspectionChecklist = void 0;
exports.VehicleInspectionChecklist = {
    auxiliaryBattery: 'Auxiliary Battery',
    fireExtinguisher: 'Fire Extinguisher',
    spareWheel: 'Spare Wheel',
    hydraulicJack: 'Hydraulic Jack',
    rightSideMirror: 'Right Side Mirror',
    leftSideMirror: 'Left Side Mirror',
    vehicleKey: 'Vehicle Key',
    toolKit: 'Tool Kit',
};
//# sourceMappingURL=vehicle-inspection-check-list.const.js.map