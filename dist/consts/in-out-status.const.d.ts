export declare const InOutStatus: {
    readonly IN: "IN";
    readonly OUT: "OUT";
};
export type InOutStatusType = keyof typeof InOutStatus;
//# sourceMappingURL=in-out-status.const.d.ts.map