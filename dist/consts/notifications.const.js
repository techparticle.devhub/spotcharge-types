"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Notifications = void 0;
exports.Notifications = {
    scheduleBooking: "scheduleBooking",
    cancelCharging: "cancelCharging",
    refundCharging: "refundCharging",
    stopCharging: "stopCharging",
    updateApp: "updateApp",
    others: "others",
    marketing: "marketing",
};
//# sourceMappingURL=notifications.const.js.map