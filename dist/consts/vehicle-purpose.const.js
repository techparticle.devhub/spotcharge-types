"use strict";
// export const VehiclePurpose = {
//     General: "General",
//     Delivery: "Delivery",
//     Maintenance: "Maintenance",
//     Charging: "Charging",
//     Inspection: "Inspection",
//     Emergency: "Emergency"
// } as const;
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehiclePurpose = void 0;
exports.VehiclePurpose = {
    GENERAL: {
        id: 'general',
        name: 'General',
    },
    DELIVERY: {
        id: 'delivery',
        name: 'Delivery',
    },
    MAINTENANCE: {
        id: 'maintenance',
        name: 'Maintenance',
    },
    CHARGING: {
        id: 'charging',
        name: 'Charging',
    },
    INSPECTION: {
        id: 'inspection',
        name: 'Inspection',
    },
    EMERGENCY: {
        id: 'emergency',
        name: 'Emergency',
    },
};
//# sourceMappingURL=vehicle-purpose.const.js.map