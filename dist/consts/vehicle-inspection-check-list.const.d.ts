export declare const VehicleInspectionChecklist: {
    auxiliaryBattery: string;
    fireExtinguisher: string;
    spareWheel: string;
    hydraulicJack: string;
    rightSideMirror: string;
    leftSideMirror: string;
    vehicleKey: string;
    toolKit: string;
};
export type VehicleInspectionChecklistType = keyof typeof VehicleInspectionChecklist;
//# sourceMappingURL=vehicle-inspection-check-list.const.d.ts.map