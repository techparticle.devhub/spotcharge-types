"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.permissionConfig = void 0;
exports.permissionConfig = {
    'VIEW_FLEET_DASHBOARD': {
        key: 'VIEW_FLEET_DASHBOARD',
        formattedPermission: 'Can View Fleet Dashboard',
    },
    'VIEW_PUBLIC_DASHBOARD': {
        key: 'VIEW_PUBLIC_DASHBOARD',
        formattedPermission: 'Can View Public Dashboard',
    },
    'LIST_FRANCHISES': {
        key: 'LIST_FRANCHISES',
        formattedPermission: 'List Franchises',
    },
    'ADD_FRANCHISE': {
        key: 'ADD_FRANCHISE',
        formattedPermission: 'Can Add Franchise',
    },
    'EDIT_FRANCHISE_SETTING': {
        key: 'EDIT_FRANCHISE_SETTING',
        formattedPermission: 'Can Edit Franchise Setting',
    },
    'EDIT_FRANCHISE_ACCOUNT_SETTING': {
        key: 'EDIT_FRANCHISE_ACCOUNT_SETTING',
        formattedPermission: 'Can Edit Franchise Account Setting',
    },
    'EDIT_FRANCHISE_PROFILE': {
        key: 'EDIT_FRANCHISE_PROFILE',
        formattedPermission: 'Can Edit Franchise Profile',
    },
    'FRANCHISE_BILLING': {
        key: 'FRANCHISE_BILLING',
        formattedPermission: 'Can View Franchise Billing',
    },
    'VIEW_FRANCHISE_FLEET_INVOICE': {
        key: 'VIEW_FRANCHISE_FLEET_INVOICE',
        formattedPermission: 'Can View Franchise Fleet Invoice',
    },
    'FRANCHISE_ORDERS': {
        key: 'FRANCHISE_ORDERS',
        formattedPermission: 'Can View Franchise Orders',
    },
    'VIEW_FRANCHISE_FLEET_ORDER': {
        key: 'VIEW_FRANCHISE_FLEET_ORDER',
        formattedPermission: 'Can View Franchise Fleet Order',
    },
    'FRANCHISE_SUBSCRIPTION': {
        key: 'FRANCHISE_SUBSCRIPTION',
        formattedPermission: 'Can Edit Franchise Subscription',
    },
    'LIST_FLEETS': {
        key: 'LIST_FLEETS',
        formattedPermission: 'List Fleets',
    },
    'ADD_FLEET': {
        key: 'ADD_FLEET',
        formattedPermission: 'Can Add Fleet',
    },
    'EDIT_FLEET': {
        key: 'EDIT_FLEET',
        formattedPermission: 'Can Edit Fleet',
    },
    'DELETE_FLEET': {
        key: 'DELETE_FLEET',
        formattedPermission: 'Can Delete Fleet',
    },
    'LIST_FLEET_USERS': {
        key: 'LIST_FLEET_USERS',
        formattedPermission: 'List Fleet Users',
    },
    'ADD_FLEET_USER': {
        key: 'ADD_FLEET_USER',
        formattedPermission: 'Can Add Fleet User',
    },
    'EDIT_FLEET_USER': {
        key: 'EDIT_FLEET_USER',
        formattedPermission: 'Can Edit Fleet User',
    },
    'DELETE_FLEET_USER': {
        key: 'DELETE_FLEET_USER',
        formattedPermission: 'Can Delete Fleet User',
    },
    'LIST_FLEET_OPERATORS': {
        key: 'LIST_FLEET_OPERATORS',
        formattedPermission: 'List Fleet Operators',
    },
    'ADD_FLEET_OPERATOR': {
        key: 'ADD_FLEET_OPERATOR',
        formattedPermission: 'Can Add Fleet Operator',
    },
    'EDIT_FLEET_OPERATOR': {
        key: 'EDIT_FLEET_OPERATOR',
        formattedPermission: 'Can Edit Fleet Operator',
    },
    'DELETE_FLEET_OPERATOR': {
        key: 'DELETE_FLEET_OPERATOR',
        formattedPermission: 'Can Delete Fleet Operator',
    },
    'LIST_FLEET_VEHICLES': {
        key: 'LIST_FLEET_VEHICLES',
        formattedPermission: 'List Fleet Vehicles',
    },
    'ADD_FLEET_VEHICLE': {
        key: 'ADD_FLEET_VEHICLE',
        formattedPermission: 'Can Add Fleet Vehicle',
    },
    'EDIT_FLEET_VEHICLE': {
        key: 'EDIT_FLEET_VEHICLE',
        formattedPermission: 'Can Edit Fleet Vehicle',
    },
    'DELETE_FLEET_VEHICLE': {
        key: 'DELETE_FLEET_VEHICLE',
        formattedPermission: 'Can Delete Fleet Vehicle',
    },
    'LIST_CHARGER_ERRORS': {
        key: 'LIST_CHARGER_ERRORS',
        formattedPermission: 'List Charger Errors',
    },
    'VIEW_CHARGER_ERROR': {
        key: 'VIEW_CHARGER_ERROR',
        formattedPermission: 'Can View Charger Error',
    },
    'DOWNLOAD_CHARGER_ERROR_LOG': {
        key: 'DOWNLOAD_CHARGER_ERROR',
        formattedPermission: 'Can Download Charger Error Log',
    },
    'LIST_FLEET_ORDER_SCHEDULES': {
        key: 'LIST_FLEET_ORDER_SCHEDULES',
        formattedPermission: 'List Fleet Order Schedules',
    },
    'VIEW_FLEET_ORDER_SCHEDULE': {
        key: 'VIEW_FLEET_ORDER_SCHEDULE',
        formattedPermission: 'Can View Fleet Order Schedule',
    },
    'STOP_FLEET_ORDER_SCHEDULE': {
        key: 'STOP_FLEET_ORDER_SCHEDULE',
        formattedPermission: 'Can Stop Fleet Order Schedule',
    },
    'RESTART_FLEET_ORDER_SCHEDULE': {
        key: 'RESTART_FLEET_ORDER_SCHEDULE',
        formattedPermission: 'Can Restart Fleet Order Schedule',
    },
    'LIST_PUBLIC_BOOKINGS': {
        key: 'LIST_PUBLIC_BOOKINGS',
        formattedPermission: 'List Public Bookings',
    },
    'VIEW_PUBLIC_BOOKING': {
        key: 'VIEW_PUBLIC_BOOKING',
        formattedPermission: 'Can View Public Booking',
    },
    'START_PUBLIC_BOOKING': {
        key: 'START_PUBLIC_BOOKING',
        formattedPermission: 'Can Start Public Booking',
    },
    'CANCEL_PUBLIC_BOOKING': {
        key: 'CANCEL_PUBLIC_BOOKING',
        formattedPermission: 'Can Cancel Public Booking',
    },
    'ISSUE_BOOKING_REFUND': {
        key: 'ISSUE_BOOKING_REFUND',
        formattedPermission: 'Can Issue Booking Refund',
    },
    'LIST_PUBLIC_INVOICES': {
        key: 'LIST_PUBLIC_INVOICES',
        formattedPermission: 'List Public Invoices',
    },
    'VIEW_PUBLIC_INVOICE': {
        key: 'VIEW_PUBLIC_INVOICE',
        formattedPermission: 'Can View Public Invoice',
    },
    'DOWNLOAD_PUBLIC_INVOICE': {
        key: 'DOWNLOAD_PUBLIC_INVOICE',
        formattedPermission: 'Can Download Public Invoice',
    },
    'PUBLIC_INVOICE_DOWNLOAD_REQUESTS': {
        key: 'PUBLIC_INVOICE_DOWNLOAD_REQUESTS',
        formattedPermission: 'Public Invoice Download Requests',
    },
    'LIST_PUBLIC_USERS': {
        key: 'LIST_PUBLIC_USERS',
        formattedPermission: 'List Public Users',
    },
    'VIEW_PUBLIC_USER': {
        key: 'VIEW_PUBLIC_USER',
        formattedPermission: 'Can View Public User',
    },
    'PUBLIC_USER_SUMMARY': {
        key: 'PUBLIC_USER_SUMMARY',
        formattedPermission: 'Can View Public User Summary',
    },
    'PUBLIC_USER_DETAIL': {
        key: 'PUBLIC_USER_DETAIL',
        formattedPermission: 'Can View Public User Detail',
    },
    'PUBLIC_USER_BOOKING': {
        key: 'PUBLIC_USER_BOOKING',
        formattedPermission: 'Can View Public User Booking',
    },
    'PUBLIC_USER_INVOICE': {
        key: 'PUBLIC_USER_INVOICE',
        formattedPermission: 'Can View Public User Invoice',
    },
    'PUBLIC_USER_WALLET': {
        key: 'PUBLIC_USER_WALLET',
        formattedPermission: 'Can View Public User Wallet',
    },
    'LIST_PUBLIC_SCHEDULES': {
        key: 'LIST_PUBLIC_SCHEDULES',
        formattedPermission: 'List Public Schedules',
    },
    'VIEW_PUBLIC_SCHEDULE': {
        key: 'VIEW_PUBLIC_SCHEDULE',
        formattedPermission: 'Can View Public Schedule',
    },
    'RESUME_PUBLIC_SCHEDULE': {
        key: 'Can Resume Public Schedule',
        formattedPermission: 'Can Resume Public Schedule',
    },
    'STOP_PUBLIC_SCHEDULE': {
        key: 'STOP_PUBLIC_SCHEDULE',
        formattedPermission: 'Can Stop Public Schedule',
    },
    'LIST_CHARGERS': {
        key: 'LIST_CHARGERS',
        formattedPermission: 'List Chargers',
    },
    'ADD_CHARGER': {
        key: 'ADD_CHARGER',
        formattedPermission: 'Can Add Charger',
    },
    'EDIT_CHARGER': {
        key: 'EDIT_CHARGER',
        formattedPermission: 'Can Edit Charger',
    },
    'DELETE_CHARGER': {
        key: 'DELETE_CHARGER',
        formattedPermission: 'Can Delete Charger',
    },
    'LIST_CHARGER_CAPACITIES': {
        key: 'LIST_CHARGER_CAPACITIES',
        formattedPermission: 'List Charger Capacities',
    },
    'ADD_CHARGER_CAPACITY': {
        key: 'ADD_CHARGER_CAPACITY',
        formattedPermission: 'Can Add Charger Capacity',
    },
    'EDIT_CHARGER_CAPACITY': {
        key: 'EDIT_CHARGER_CAPACITY',
        formattedPermission: 'Can Edit Charger Capacity',
    },
    'DELETE_CHARGER_CAPACITY': {
        key: 'DELETE_CHARGER_CAPACITY',
        formattedPermission: 'Can Delete Charger Capacity',
    },
    'VIEW_FLEET_ORDER_REPORT': {
        key: 'VIEW_FLEET_ORDER_REPORT',
        formattedPermission: 'Can View Fleet Order Report',
    },
    'EXPORT_FLEET_ORDER_REPORT': {
        key: 'EXPORT_FLEET_ORDER_REPORT',
        formattedPermission: 'Can Export Fleet Order Report',
    },
    'VIEW_ORDERS_BY_FLEET_VEHICLE_REPORT': {
        key: 'VIEW_ORDERS_BY_FLEET_VEHICLE_REPORT',
        formattedPermission: 'Can View Orders By Fleet Vehicle Report',
    },
    'EXPORT_ORDERS_BY_FLEET_VEHICLE_REPORT': {
        key: 'EXPORT_ORDERS_BY_FLEET_VEHICLE_REPORT',
        formattedPermission: 'Can Export Orders By Fleet Vehicle Report',
    },
    'VIEW_ORDERS_BY_USER_REPORT': {
        key: 'VIEW_ORDERS_BY_USER_REPORT',
        formattedPermission: 'Can View Orders By User Report',
    },
    'EXPORT_ORDERS_BY_USER_REPORT': {
        key: 'EXPORT_ORDERS_BY_USER_REPORT',
        formattedPermission: 'Can Orders By User Report',
    },
    'VIEW_PUBLIC_ORDER_REPORT': {
        key: 'VIEW_PUBLIC_ORDER_REPORT',
        formattedPermission: 'Can View Public Order Report',
    },
    'EXPORT_PUBLIC_ORDER_REPORT': {
        key: 'EXPORT_PUBLIC_ORDER_REPORT',
        formattedPermission: 'Can Export Public Order Report',
    },
    'VIEW_ORDER_BY_PUBLIC_USER_REPORT': {
        key: 'VIEW_ORDER_BY_PUBLIC_USER_REPORT',
        formattedPermission: 'Can View Order By Public User Report',
    },
    'EXPORT_ORDER_BY_PUBLIC_USER_REPORT': {
        key: 'EXPORT_ORDER_BY_PUBLIC_USER_REPORT',
        formattedPermission: 'Can Export Order By Public User Report',
    },
    'VIEW_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT': {
        key: 'VIEW_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT',
        formattedPermission: 'Can View Order By Public User Vehicle Report',
    },
    'EXPORT_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT': {
        key: 'EXPORT_ORDER_BY_PUBLIC_USER_VEHICLE_REPORT',
        formattedPermission: 'Can Export Order By Public User Vehicle Report',
    },
    'VIEW_ORDER_BY_PUBLIC_CHARGER_REPORT': {
        key: 'VIEW_ORDER_BY_PUBLIC_CHARGER_REPORT',
        formattedPermission: 'Can View Order By Public Charger Report',
    },
    'EXPORT_ORDER_BY_PUBLIC_CHARGER_REPORT': {
        key: 'EXPORT_ORDER_BY_PUBLIC_CHARGER_REPORT',
        formattedPermission: 'Can Export Order By Public Charger Report',
    },
    'VIEW_FLEET_VEHICLE_REPORT': {
        key: 'VIEW_FLEET_VEHICLE_REPORT',
        formattedPermission: 'Can View Fleet Vehicle Report',
    },
    'EXPORT_FLEET_VEHICLE_REPORT': {
        key: 'EXPORT_FLEET_VEHICLE_REPORT',
        formattedPermission: 'Can Export Fleet Vehicle Report',
    },
    'VIEW_FLEET_VEHICLE_IN_OUT_REPORT': {
        key: 'VIEW_FLEET_VEHICLE_IN_OUT_REPORT',
        formattedPermission: 'Can View Fleet Vehicle In Out Report',
    },
    'EXPORT_FLEET_VEHICLE_IN_OUT_REPORT': {
        key: 'EXPORT_FLEET_VEHICLE_IN_OUT_REPORT',
        formattedPermission: 'Can Export Fleet Vehicle In Out Report',
    },
    'VIEW_ASSET_IN_OUT_REPORT': {
        key: 'VIEW_ASSET_IN_OUT_REPORT',
        formattedPermission: 'Can View Asset In Out Report',
    },
    'EXPORT_ASSET_IN_OUT_REPORT': {
        key: 'EXPORT_ASSET_IN_OUT_REPORT',
        formattedPermission: 'Can Export Asset In Out Report',
    },
    'VIEW_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT': {
        key: 'VIEW_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT',
        formattedPermission: 'Can View Fleet Vehicle In Out Summary Report',
    },
    'EXPORT_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT': {
        key: 'EXPORT_FLEET_VEHICLE_IN_OUT_SUMMARY_REPORT',
        formattedPermission: 'Can Export Fleet Vehicle In Out Summary Report',
    },
    'VIEW_ASSET_IN_OUT_SUMMARY_REPORT': {
        key: 'VIEW_ASSET_IN_OUT_SUMMARY_REPORT',
        formattedPermission: 'Can View Asset In Out Summary Report',
    },
    'EXPORT_ASSET_IN_OUT_SUMMARY_REPORT': {
        key: 'EXPORT_ASSET_IN_OUT_SUMMARY_REPORT',
        formattedPermission: 'Can Export Asset In Out Summary Report',
    },
    'VIEW_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT': {
        key: 'VIEW_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT',
        formattedPermission: 'Can View Fleet Vehicle In Out Difference Report',
    },
    'EXPORT_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT': {
        key: 'EXPORT_FLEET_VEHICLE_IN_OUT_DIFFERENCE_REPORT',
        formattedPermission: 'Can Export Fleet Vehicle In Out Difference Report',
    },
    'LIST_CONNECTOR_TYPES': {
        key: 'LIST_CONNECTOR_TYPES',
        formattedPermission: 'List Connector Types',
    },
    'ADD_CONNECTOR_TYPE': {
        key: 'ADD_CONNECTOR_TYPE',
        formattedPermission: 'Can Add Connector Type',
    },
    'EDIT_CONNECTOR_TYPE': {
        key: 'EDIT_CONNECTOR_TYPE',
        formattedPermission: 'Can Edit Connector Type',
    },
    'DELETE_CONNECTOR_TYPE': {
        key: 'DELETE_CONNECTOR_TYPE',
        formattedPermission: 'Can Delete Connector Type',
    },
    'LIST_VENDORS': {
        key: 'LIST_VENDORS',
        formattedPermission: 'List Vendors',
    },
    'ADD_VENDOR': {
        key: 'ADD_VENDOR',
        formattedPermission: 'Can Add Vendor',
    },
    'EDIT_VENDOR': {
        key: 'EDIT_VENDOR',
        formattedPermission: 'Can Edit Vendor',
    },
    'LIST_RFIDS': {
        key: 'LIST_RFIDS',
        formattedPermission: 'List Rfids',
    },
    'ADD_RFID': {
        key: 'ADD_RFID',
        formattedPermission: 'Can Add RFID',
    },
    'EDIT_RFID': {
        key: 'EDIT_RFID',
        formattedPermission: 'Can Edit RFID',
    },
    'DELETE_RFID': {
        key: 'DELETE_RFID',
        formattedPermission: 'Can Delete RFID',
    },
    'LIST_VEHICLES': {
        key: 'LIST_VEHICLES',
        formattedPermission: 'List Vehicles',
    },
    'ADD_VEHICLE': {
        key: 'ADD_VEHICLE',
        formattedPermission: 'Can Add Vehicle',
    },
    'EDIT_VEHICLE': {
        key: 'EDIT_VEHICLE',
        formattedPermission: 'Can Edit Vehicle',
    },
    'DELETE_VEHICLE': {
        key: 'DELETE_VEHICLE',
        formattedPermission: 'Can Delete Vehicle',
    },
    'LIST_BRANDS': {
        key: 'LIST_BRANDS',
        formattedPermission: 'List Brands',
    },
    'ADD_BRAND': {
        key: 'ADD_BRAND',
        formattedPermission: 'Can Add BRAND',
    },
    'EDIT_BRAND': {
        key: 'EDIT_BRAND',
        formattedPermission: 'Can Edit BRAND',
    },
    'DELETE_BRAND': {
        key: 'DELETE_BRAND',
        formattedPermission: 'Can Delete BRAND',
    },
    'LIST_LOGS': {
        key: 'LIST_LOGS',
        formattedPermission: 'List Logs',
    },
    'EXPORT_LOG_REPORTS': {
        key: 'EXPORT_LOG_REPORTS',
        formattedPermission: 'Can Export Log Reports',
    },
    'LIST_SERVICE_CHARGES': {
        key: 'LIST_SERVICE_CHARGES',
        formattedPermission: 'List Service Charges',
    },
    'ADD_SERVICE_CHARGE': {
        key: 'ADD_SERVICE_CHARGE',
        formattedPermission: 'Can Add Service Charge',
    },
    'EDIT_SERVICE_CHARGE': {
        key: 'EDIT_SERVICE_CHARGE',
        formattedPermission: 'Can Edit Service Charge',
    },
    'DELETE_SERVICE_CHARGE': {
        key: 'DELETE_SERVICE_CHARGE',
        formattedPermission: 'Can Delete Service Charge',
    },
    'LIST_TAX_RATES': {
        key: 'LIST_TAX_RATES',
        formattedPermission: 'List Tax Rates',
    },
    'ADD_TAX_RATE': {
        key: 'ADD_TAX_RATE',
        formattedPermission: 'Can Add Tax Rate',
    },
    'EDIT_TAX_RATE': {
        key: 'EDIT_TAX_RATE',
        formattedPermission: 'Can Edit Tax Rate',
    },
    'DELETE_TAX_RATE': {
        key: 'DELETE_TAX_RATE',
        formattedPermission: 'Can Delete Tax Rate',
    },
    'LIST_GST': {
        key: 'LIST_GST',
        formattedPermission: 'List GST',
    },
    'ADD_GST': {
        key: 'ADD_GST',
        formattedPermission: 'Can Add GST',
    },
    'EDIT_GST': {
        key: 'EDIT_GST',
        formattedPermission: 'Can Edit GST',
    },
    'DELETE_GST': {
        key: 'DELETE_GST',
        formattedPermission: 'Can Delete GST',
    },
    'LIST_SUPPORT_QUERY': {
        key: 'LIST_SUPPORT_QUERY',
        formattedPermission: 'List Support Query',
    },
    'VIEW_SUPPORT_QUERY': {
        key: 'VIEW_SUPPORT_QUERY',
        formattedPermission: 'Can View Support Query',
    },
    'LIST_USER_ROLE': {
        key: 'LIST_USER_ROLE',
        formattedPermission: 'List User Role',
    },
    'ADD_USER_ROLE': {
        key: 'ADD_USER_ROLE',
        formattedPermission: 'Can Add User Role',
    },
    'EDIT_USER_ROLE': {
        key: 'EDIT_USER_ROLE',
        formattedPermission: 'Can Edit User Role',
    },
    'LIST_PERMISSION_GROUP': {
        key: 'LIST_PERMISSION_GROUP',
        formattedPermission: 'List Permission Group',
    },
    'ADD_PERMISSION_GROUP': {
        key: 'ADD_PERMISSION_GROUP',
        formattedPermission: 'Can Add Permission Group',
    },
    'EDIT_PERMISSION_GROUP': {
        key: 'EDIT_PERMISSION_GROUP',
        formattedPermission: 'Can Edit Permission Group',
    },
    'DELETE_PERMISSION_GROUP': {
        key: 'DELETE_PERMISSION_GROUP',
        formattedPermission: 'Can Delete Permission Group',
    },
    'LIST_ADMIN_USERS': {
        key: 'LIST_ADMIN_USERS',
        formattedPermission: 'List Admin Users',
    },
    'ADD_ADMIN_USER': {
        key: 'ADD_ADMIN_USER',
        formattedPermission: 'Can Add Admin User',
    },
    'EDIT_ADMIN_USER': {
        key: 'EDIT_ADMIN_USER',
        formattedPermission: 'Can Edit Admin User',
    },
    'DELETE_ADMIN_USER': {
        key: 'DELETE_ADMIN_USER',
        formattedPermission: 'Can Delete Admin User',
    }
};
//# sourceMappingURL=permission-config.js.map