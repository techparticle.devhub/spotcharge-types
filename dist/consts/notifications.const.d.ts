export declare const Notifications: {
    readonly scheduleBooking: "scheduleBooking";
    readonly cancelCharging: "cancelCharging";
    readonly refundCharging: "refundCharging";
    readonly stopCharging: "stopCharging";
    readonly updateApp: "updateApp";
    readonly others: "others";
    readonly marketing: "marketing";
};
export type NotificationType = keyof typeof Notifications;
//# sourceMappingURL=notifications.const.d.ts.map