export declare enum PaymentStatus {
    Paid = "paid",
    UnPaid = "unPaid",
    Failed = "failed",
    Prepayment = "prepayment",
    PartialPaid = "partialPaid"
}
//# sourceMappingURL=payment-status.d.ts.map