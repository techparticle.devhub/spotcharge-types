export declare enum ChargerStatus {
    Active = "active",
    Inactive = "inactive",
    InMaintenance = "in-maintenance"
}
//# sourceMappingURL=charger-status-type.enum.d.ts.map