"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PointType = void 0;
var PointType;
(function (PointType) {
    PointType["DC"] = "dc";
    PointType["AC"] = "ac";
    PointType["Hybrid"] = "hybrid";
})(PointType = exports.PointType || (exports.PointType = {}));
//# sourceMappingURL=point-type.enum.js.map