"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = void 0;
var Roles;
(function (Roles) {
    Roles["SuperAdmin"] = "super_admin";
    Roles["FranchiseAdmin"] = "franchise_admin";
    Roles["FleetUser"] = "fleet_user";
    Roles["FleetOperator"] = "fleet_operator";
})(Roles = exports.Roles || (exports.Roles = {}));
//# sourceMappingURL=roles.enum.js.map