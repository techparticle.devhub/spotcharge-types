"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleType = void 0;
var VehicleType;
(function (VehicleType) {
    VehicleType["Car"] = "car";
    VehicleType["Bike"] = "bike";
    VehicleType["Rikshaw"] = "rikshaw";
    VehicleType["Pickup"] = "Pickup";
})(VehicleType = exports.VehicleType || (exports.VehicleType = {}));
//# sourceMappingURL=vehicle-type.enum.js.map