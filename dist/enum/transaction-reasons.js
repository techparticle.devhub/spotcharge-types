"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionReason = void 0;
var TransactionReason;
(function (TransactionReason) {
    TransactionReason["REFUND"] = "refund";
    TransactionReason["REWARD"] = "reward";
    TransactionReason["PROMOTION"] = "promotion";
    TransactionReason["COMPENSATION"] = "compensation";
    TransactionReason["BONUS"] = "bonus";
    TransactionReason["DEPOSIT"] = "deposit";
    TransactionReason["WITHDRAWAL"] = "withdrawal";
    TransactionReason["CHARGING"] = "charging";
    TransactionReason["ADD_BALANCE"] = "add balance";
    TransactionReason["BOOKING"] = "booking";
    TransactionReason["BANK_REFUND"] = "bank refund";
})(TransactionReason = exports.TransactionReason || (exports.TransactionReason = {}));
//# sourceMappingURL=transaction-reasons.js.map