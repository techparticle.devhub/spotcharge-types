"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupportQueriesStatus = void 0;
var SupportQueriesStatus;
(function (SupportQueriesStatus) {
    SupportQueriesStatus["Open"] = "open";
    SupportQueriesStatus["InProgress"] = "in-progress";
    SupportQueriesStatus["Resolved"] = "resolved";
})(SupportQueriesStatus = exports.SupportQueriesStatus || (exports.SupportQueriesStatus = {}));
//# sourceMappingURL=support-queries-status.js.map