"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChargerLocationType = void 0;
var ChargerLocationType;
(function (ChargerLocationType) {
    ChargerLocationType["Public"] = "public";
    ChargerLocationType["Private"] = "private";
})(ChargerLocationType = exports.ChargerLocationType || (exports.ChargerLocationType = {}));
//# sourceMappingURL=charger-location-type.enum.js.map