export declare enum VehicleType {
    Car = "car",
    Bike = "bike",
    Rikshaw = "rikshaw",
    Pickup = "Pickup"
}
//# sourceMappingURL=vehicle-type.enum.d.ts.map