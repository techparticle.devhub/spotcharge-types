export declare enum SupportQueriesStatus {
    Open = "open",
    InProgress = "in-progress",
    Resolved = "resolved"
}
//# sourceMappingURL=support-queries-status.d.ts.map