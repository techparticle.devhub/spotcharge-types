export declare enum TransactionType {
    CREDIT = "credit",
    DEBIT = "debit"
}
//# sourceMappingURL=transaction-type.d.ts.map