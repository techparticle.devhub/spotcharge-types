export declare enum PointType {
    DC = "dc",
    AC = "ac",
    Hybrid = "hybrid"
}
//# sourceMappingURL=point-type.enum.d.ts.map