"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaxCategory = void 0;
var TaxCategory;
(function (TaxCategory) {
    TaxCategory["Individual"] = "individual";
    TaxCategory["Group"] = "group";
})(TaxCategory = exports.TaxCategory || (exports.TaxCategory = {}));
//# sourceMappingURL=tax-category.js.map