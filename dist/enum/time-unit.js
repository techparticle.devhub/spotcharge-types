"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeUnit = void 0;
var TimeUnit;
(function (TimeUnit) {
    TimeUnit["Minute"] = "minute";
    TimeUnit["Hour"] = "hour";
})(TimeUnit = exports.TimeUnit || (exports.TimeUnit = {}));
//# sourceMappingURL=time-unit.js.map