"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorStatus = void 0;
var ConnectorStatus;
(function (ConnectorStatus) {
    ConnectorStatus["Available"] = "Available";
    ConnectorStatus["Preparing"] = "Preparing";
    ConnectorStatus["Charging"] = "Charging";
    ConnectorStatus["SuspendedEV"] = "SuspendedEV";
    ConnectorStatus["SuspendedEVSE"] = "SuspendedEVSE";
    ConnectorStatus["Finishing"] = "Finishing";
    ConnectorStatus["Reserved"] = "Reserved";
    ConnectorStatus["Unavailable"] = "Unavailable";
    ConnectorStatus["Faulted"] = "Faulted";
})(ConnectorStatus = exports.ConnectorStatus || (exports.ConnectorStatus = {}));
//# sourceMappingURL=connector-status.enum.js.map