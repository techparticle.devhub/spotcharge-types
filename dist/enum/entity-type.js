"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityType = void 0;
var EntityType;
(function (EntityType) {
    EntityType["client"] = "client";
    EntityType["business"] = "business";
    EntityType["system"] = "system";
    EntityType["superAdmin"] = "superAdmin";
    EntityType["franchise"] = "franchise";
    EntityType["fleetApp"] = "fleetApp";
    EntityType["publicApp"] = "publicApp";
    EntityType["fleetApi"] = "fleetApi";
    EntityType["publicApi"] = "publicApi";
})(EntityType = exports.EntityType || (exports.EntityType = {}));
//# sourceMappingURL=entity-type.js.map