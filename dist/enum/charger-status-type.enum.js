"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChargerStatus = void 0;
var ChargerStatus;
(function (ChargerStatus) {
    ChargerStatus["Active"] = "active";
    ChargerStatus["Inactive"] = "inactive";
    ChargerStatus["InMaintenance"] = "in-maintenance";
})(ChargerStatus = exports.ChargerStatus || (exports.ChargerStatus = {}));
//# sourceMappingURL=charger-status-type.enum.js.map