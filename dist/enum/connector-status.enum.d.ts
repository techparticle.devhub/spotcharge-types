export declare enum ConnectorStatus {
    Available = "Available",
    Preparing = "Preparing",
    Charging = "Charging",
    SuspendedEV = "SuspendedEV",
    SuspendedEVSE = "SuspendedEVSE",
    Finishing = "Finishing",
    Reserved = "Reserved",
    Unavailable = "Unavailable",
    Faulted = "Faulted"
}
//# sourceMappingURL=connector-status.enum.d.ts.map