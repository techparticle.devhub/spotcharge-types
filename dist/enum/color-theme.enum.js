"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColorTheme = void 0;
// eslint-disable-next-line no-shadow
var ColorTheme;
(function (ColorTheme) {
    ColorTheme["Dark"] = "dark";
    ColorTheme["Light"] = "light";
})(ColorTheme = exports.ColorTheme || (exports.ColorTheme = {}));
//# sourceMappingURL=color-theme.enum.js.map