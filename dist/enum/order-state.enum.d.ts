export declare enum OrderState {
    Initiated = "initiated",
    Started = "started",
    InProgress = "inProgress",
    Completed = "completed"
}
//# sourceMappingURL=order-state.enum.d.ts.map