"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorStatus = void 0;
var ConnectorStatus;
(function (ConnectorStatus) {
    ConnectorStatus["Active"] = "active";
    ConnectorStatus["Inactive"] = "inactive";
    ConnectorStatus["InMaintenance"] = "in-maintenance";
})(ConnectorStatus = exports.ConnectorStatus || (exports.ConnectorStatus = {}));
//# sourceMappingURL=connector-status-type.enum.js.map