"use strict";
// eslint-disable-next-line no-shadow
Object.defineProperty(exports, "__esModule", { value: true });
exports.Collection = void 0;
var Collection;
(function (Collection) {
    Collection["Franchises"] = "franchises";
    Collection["PublicUsers"] = "publicUsers";
    Collection["Users"] = "users";
    Collection["TempAssignClaim"] = "tempAssignClaim";
    Collection["Fleets"] = "fleets";
    Collection["FleetUsers"] = "fleetUsers";
    Collection["Chargers"] = "chargers";
    Collection["FleetChargers"] = "fleetChargers";
    Collection["ChargerCapacities"] = "chargerCapacities";
    Collection["Vendors"] = "vendors";
    Collection["ConnectorTypes"] = "connectorTypes";
    Collection["Orders"] = "orders";
    Collection["Rfids"] = "rfids";
    Collection["FleetInvoices"] = "fleetInvoices";
    Collection["Invoices"] = "invoices";
    Collection["DeleteAccounts"] = "deleteAccounts";
    Collection["PublicUserDeleteAccounts"] = "publicUserDeleteAccounts";
    Collection["FleetOrders"] = "fleetOrders";
    Collection["Bookings"] = "bookings";
    Collection["WalletTransaction"] = "walletTransaction";
    Collection["FleetUserFranchiseChangeLogs"] = "fleetUserFranchiseChangeLogs";
    Collection["ServiceCharges"] = "serviceCharges";
    Collection["FleetOperators"] = "fleetOperators";
    Collection["Brands"] = "brands";
    Collection["SupportQueries"] = "supportQueries";
    Collection["versionInfo"] = "versionInfo";
    Collection["ChargerErrors"] = "chargerErrors";
    Collection["FleetVehicle"] = "fleetVehicle";
    Collection["InOuts"] = "inOuts";
    Collection["Assets"] = "assets";
    Collection["GstRecords"] = "gstRecords";
    Collection["Notifications"] = "notifications";
    Collection["MasterUserRoles"] = "masterUserRoles";
    Collection["PermissionGroups"] = "permissionGroups";
    Collection["AdminUsers"] = "adminUsers";
})(Collection = exports.Collection || (exports.Collection = {}));
//# sourceMappingURL=firestore-collections.enum.js.map