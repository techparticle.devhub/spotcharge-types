export declare enum PaymentMethod {
    Wallet = "wallet",
    TopUp = "top_up",
    PartiallyWallet = "partially_wallet"
}
//# sourceMappingURL=payment-method.d.ts.map