"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentMethod = void 0;
var PaymentMethod;
(function (PaymentMethod) {
    PaymentMethod["Wallet"] = "wallet";
    PaymentMethod["TopUp"] = "top_up";
    PaymentMethod["PartiallyWallet"] = "partially_wallet";
})(PaymentMethod = exports.PaymentMethod || (exports.PaymentMethod = {}));
//# sourceMappingURL=payment-method.js.map