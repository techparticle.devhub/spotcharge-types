"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChargerStatus = void 0;
var ChargerStatus;
(function (ChargerStatus) {
    ChargerStatus["Online"] = "online";
    ChargerStatus["Offline"] = "offline";
})(ChargerStatus = exports.ChargerStatus || (exports.ChargerStatus = {}));
//# sourceMappingURL=charger-status.enum.js.map