"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingStatus = void 0;
var BookingStatus;
(function (BookingStatus) {
    BookingStatus["CONFIRMED"] = "confirmed";
    BookingStatus["CANCELLED"] = "cancelled";
    BookingStatus["COMPLETED"] = "completed";
    BookingStatus["PENDING"] = "pending";
    BookingStatus["IN_PROGRESS"] = "in_progress";
    BookingStatus["PAUSED"] = "paused";
})(BookingStatus = exports.BookingStatus || (exports.BookingStatus = {}));
//# sourceMappingURL=book-status.enum.js.map