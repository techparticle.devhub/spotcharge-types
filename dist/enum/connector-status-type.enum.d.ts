export declare enum ConnectorStatus {
    Active = "active",
    Inactive = "inactive",
    InMaintenance = "in-maintenance"
}
//# sourceMappingURL=connector-status-type.enum.d.ts.map