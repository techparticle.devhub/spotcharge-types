"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentStatus = void 0;
var PaymentStatus;
(function (PaymentStatus) {
    PaymentStatus["Paid"] = "paid";
    PaymentStatus["UnPaid"] = "unPaid";
    PaymentStatus["Failed"] = "failed";
    PaymentStatus["Prepayment"] = "prepayment";
    PaymentStatus["PartialPaid"] = "partialPaid";
})(PaymentStatus = exports.PaymentStatus || (exports.PaymentStatus = {}));
//# sourceMappingURL=payment-status.js.map