"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChargerType = void 0;
var ChargerType;
(function (ChargerType) {
    ChargerType["Static"] = "static";
    ChargerType["Dynamic"] = "dynamic";
})(ChargerType = exports.ChargerType || (exports.ChargerType = {}));
//# sourceMappingURL=charger-type.enum.js.map