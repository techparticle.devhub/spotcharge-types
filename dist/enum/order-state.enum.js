"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderState = void 0;
var OrderState;
(function (OrderState) {
    OrderState["Initiated"] = "initiated";
    OrderState["Started"] = "started";
    OrderState["InProgress"] = "inProgress";
    OrderState["Completed"] = "completed";
})(OrderState = exports.OrderState || (exports.OrderState = {}));
//# sourceMappingURL=order-state.enum.js.map