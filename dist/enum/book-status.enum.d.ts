export declare enum BookingStatus {
    CONFIRMED = "confirmed",
    CANCELLED = "cancelled",
    COMPLETED = "completed",
    PENDING = "pending",
    IN_PROGRESS = "in_progress",
    PAUSED = "paused"
}
//# sourceMappingURL=book-status.enum.d.ts.map