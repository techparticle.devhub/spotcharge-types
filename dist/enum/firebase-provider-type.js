"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FirebaseProviderType = void 0;
var FirebaseProviderType;
(function (FirebaseProviderType) {
    FirebaseProviderType["Google"] = "google.com";
    FirebaseProviderType["Facebook"] = "facebook.com";
    FirebaseProviderType["Password"] = "password";
})(FirebaseProviderType = exports.FirebaseProviderType || (exports.FirebaseProviderType = {}));
//# sourceMappingURL=firebase-provider-type.js.map